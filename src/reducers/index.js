
import { combineReducers } from 'redux';
import homeReducer from '../Components/Home/homeReducer';
import searchReducer from '../Components/Search/searchReducer';


const rootReducer = combineReducers({
  home: homeReducer,
  search: searchReducer,

});

export default rootReducer;