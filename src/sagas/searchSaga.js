import { takeEvery, call, put, all } from "redux-saga/effects";
import { getContacts } from '../services'
// import { fetchList, fetchListSuccess,fetchListError } from './Components/Home/homeAction'


export function* handleFetchContacts() {
  try {
    const res = yield call(getContacts);
    const data = res.data.deliveryStaffResponse.data.deliveryStaff
    yield put({type: 'FETCH_CONTACTS_SUCCESS', data: data});
  } catch (error) {
    yield put({type:'FETCH_CONTACTS_ERROR', error:error.message});
  }
}

export default function* watchFetchContacts() {
    yield takeEvery('FETCH_CONTACTS', handleFetchContacts);
  }

