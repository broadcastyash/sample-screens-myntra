import { all } from 'redux-saga/effects';
import watchFetchList from './homeSaga.js'
import watchFetchContacts from './searchSaga'

export default function* rootSaga() {
    yield all([watchFetchList(),watchFetchContacts()]);
}
