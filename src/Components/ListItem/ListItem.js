import React from 'react';
import './ListItem.css'

export default class List extends React.Component {

  handleClick(e) {
    this.props.getListItem(e);
}
  renderItem = details => (
    <div>
      {details.children.map((key, index) => (
        <div key={index}>
          <p className="names" onClick={(e)=>this.handleClick(key)}>
            {key.name}
          </p>
        </div>
      ))}
    </div>
  );

  renderList = () => {
    const initials = Object.keys(this.props.data);
    return (
      <div className="contactList">
        {initials.map((key, index) => (
          <div key={index}>
            <div>
              <p className="initial">{key.toUpperCase()}</p>
            </div>
            <div id="items">{this.renderItem(this.props.data[key])}</div>
          </div>
        ))}
      </div>
    );
  };

  render() {
   
    return <div>{this.renderList()}</div>;
  }
}
