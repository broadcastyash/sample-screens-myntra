import React from "react";
import "./Home.css";
import Card from "../SimpleCard/Card";
import { connect } from "react-redux";
import { fetchList } from "./homeAction";

class Home extends React.Component {
  componentDidMount() {
    this.props.fetchLists();
  }

  render() {
    const { data, isLoading, error } = this.props;
    if (isLoading === true) {
      return <div>Loading...</div>;
    }
    if (error != null) {
      return <div>{error} </div>;
    }
    return (
      <React.Fragment>
        {isLoading === false &&
          data &&
          data.map((e, i) => <Card data={e} key={data[i].id} />)}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.home.isLoading,
    data: state.home.data.deliveryCenterAppTripEntries,
    error: state.home.error
  };
};

const mapDispatchToProps = dispatch => ({
  fetchLists: () => dispatch(fetchList())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
