const initialState = {
  isLoading: false,
  data: [],
  error: null
};

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_LIST":
      return { ...state, ...action, isLoading: true };
    case "FETCH_LIST_SUCCESS":
      return { ...state, ...action.data, isLoading: false };
    case "FETCH_LIST_ERROR":
      console.log("error in reducer", action.error);
      return { ...state, error: action.error, isLoading: false };
    default:
      return state;
  }
};

export default homeReducer;
