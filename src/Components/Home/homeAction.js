const fetchList = () => ({
  type: "FETCH_LIST"
});

const fetchListSuccess = data => ({
  type: "FETCH_LIST_SUCCESS",
  data
});

const fetchListError = error => ({
  type: "FETCH_LIST_ERROR",
  error
});

export { fetchList, fetchListSuccess, fetchListError };
