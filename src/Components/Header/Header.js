import React from "react";
import "./Header.css";
import { Link } from "react-router-dom";

const Header = () => (
  <div className="header">
    <a href="/" className="logo">
      DC Tripsheet
    </a>

    <div className="header-right">
      <a href="sda" className="logo">
        Select SDA
      </a>
    </div>
  </div>
);

export default Header;
