const fetchContacts = () => ({
    type: 'FETCH_CONTACTS',
});

const fetchContactsSuccess = contacts => ({
    type: 'FETCH_CONTACTS_SUCCESS',
    contacts,
});

const fetchContactsError = error => ({
    type: 'FETCH_CONTACTS_ERROR',
    error,
});

export {
    fetchContacts,
    fetchContactsSuccess,
    fetchContactsError
}