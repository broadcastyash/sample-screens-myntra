
const initialState = {
  isLoading: true,
  contacts:[],
  error:null,
};

const homeReducer = (state = initialState, action) => {  
  
  switch (action.type){ 
    case 'FETCH_CONTACTS':  
      return { ...state, ...action, isLoading: true};  
    case 'FETCH_CONTACTS_SUCCESS':
      return { ...state,  contacts: action.data, isLoading: false };
    case 'FETCH_CONTACTS_ERROR':
      return { ...state, error: action.error, isLoading: false };    
    default:
      return state; 
  }
}

export default homeReducer;