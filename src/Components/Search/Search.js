import React from "react";
import { connect } from "react-redux";
import './Search.css'
import { sortBy, get } from 'lodash';
import List from '../List/List'

import { fetchContacts } from './searchAction'

class Search extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      showList: false
    }
  }

  async componentDidMount(){
    try {
      const result = await this.props.fetchContacts();
      console.log(`success: ${result}`)
  } catch (err) {
      console.log('error in search',err)
  }
  }

  renderList = () =>{
    this.setState({
      showList: true
    })
  }

  hideList = () =>{
    this.setState({
      showList: false
    })
  }

  render() { 
    console.log('list',this.props.contacts)  
   
    
    return (
        <React.Fragment>
              {this.state.showList===true && <List/>}
        </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  contacts: state.search.contacts,
  isLoading: state.search.isLoading,

});

const mapDispatchToProps = dispatch => ({
  fetchContacts: () => dispatch(fetchContacts()),
});
  
  

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Search);