import React from "react";
import { connect } from "react-redux";
import { sortBy, get } from "lodash";
import ListItem from "../ListItem/ListItem";
import { fetchContacts } from "../Search/searchAction";
import ListItemDetails from "../ListItemDetails/ListItemDetail";
import './List.css'

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialItems: [],
      items: [],
      data: "",
      list: "",
      hideList: false,
      showList: true,
      itemDetails: {}
    };
    // this.getListItem = this.getListItem.bind(this);
  }

  async componentDidMount() {
    try {
      const result = await this.props.fetchContacts();
      return result;
    } catch (err) {
      console.log("error in search", err);
    }
  }

  sortedNameGroups = list => {
    const data = list.reduce((r, e) => {
      const group = e.name[0].toLowerCase();
      if (!r[group]) r[group] = { group, children: [e] };
      else r[group].children.push(e);
      return r;
    }, []);
    this.setState({ data });
  };

  filterList = e => {
    let updatedList = this.state.initialItems;
    updatedList = updatedList.filter(
      item =>
        item.name.toLowerCase().search(e.target.value.toLowerCase()) !== -1
    );
    this.setState({ items: updatedList, list: e.target.value });
    this.sortedNameGroups(updatedList);
  };

  getListItem = e => {
    this.setState({ showList: false, itemDetails: e, list: e.name });
  };
  handleList = () => {
    this.setState({ showList: true });
  };

  componentWillReceiveProps(nextProps) {
    const cList = nextProps.contacts;
    if (cList) {
      const newList = Array.from(
        cList instanceof Array ? cList : [cList],
        item => ({
          name: `${item.firstName} ${item.lastName}`,
          id: item.id
        })
      );
      const sortList = sortBy(newList, [i => i.name.toLocaleLowerCase()]);

      this.setState({ initialItems: sortList }, () =>
        this.sortedNameGroups(this.state.initialItems)
      );
    }
  }

  render() {
    if (this.props.isLoading === true) {
      return <div>Loading...</div>;
    }

    return (
      <React.Fragment>
        <div className="search">
          <span className="fa fa-search"></span>
          <input
            placeholder="Search collections"
            value={this.state.list}
            onChange={value => this.filterList(value)}
            onFocus={this.handleList}
          />
        </div>
        {!this.state.showList && (
          <ListItemDetails details={this.state.itemDetails} />
        )}
        {this.state.showList && (
          <ListItem data={this.state.data} getListItem={this.getListItem} />
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  contacts: state.search.contacts,
  isLoading: state.search.isLoading
});

const mapDispatchToProps = dispatch => ({
  fetchContacts: () => dispatch(fetchContacts())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(List);
