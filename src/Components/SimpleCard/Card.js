import React from 'react';
import './Card.css';

const Card = (props) => {
   return(
   <div className="card">
    <div className="name">{props.data.deliveryStaffEntry.firstName}<p id="routes">{props.data.deliveryStaffEntry.routes ? props.data.deliveryStaffEntry.routes: 'Routes N/A' }</p></div>
    <div className="shipments">Shipments<p id="number">{props.data.orderCount}</p></div>
    <div className="trips">Trips Assigned</div>
 </div>
   ) 
   }

 
export default Card;
