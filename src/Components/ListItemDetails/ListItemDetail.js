import React from 'react'
import './ListItemDetails.css'

const ItemDetail = (props) => {
    return (
        <div className="details">
            <p className="det">Name: {props.details.name}</p>
            <p className="det">ID: {props.details.id}</p>
        </div>
    )
}

export default ItemDetail;