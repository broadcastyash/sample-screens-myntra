import React from 'react';
import Home from './Components/Home/Home'
import './App.css';
import configureStore from './store';
import { Provider } from 'react-redux'
import Header from './Components//Header/Header';

import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import List from './Components/List/List';



const store = configureStore();


function App() {
  return (
    <Provider store={store}>
      <Header/>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/sda" component={List} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
